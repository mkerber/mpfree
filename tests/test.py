#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Oct 21 16:05:00 2020

@author: arolle
"""

import subprocess
import os.path

test_data = ['function_rips_sample', 'multi_cover_96_points', 'points_on_sphere_15000', 
             'random_del_firep_20000_homdim_1', 'random_del_firep_20000_homdim_2']

# testing mpfree_sequential

if os.path.isfile('../mpfree_sequential'):

    print('Testing mpfree_sequential')
    
    tests = {}
    
    for data in test_data:
        
        print('Testing ' + data)
        
        input_file = '../sample_files/' + data + '.firep'
        output_file = 'min_' + data + '_test.firep'
        
        # test with default options
        
        precomputed_output_file = 'min_' + data + '.firep'
        subprocess.run(['../mpfree_sequential', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'default')] = True
        else:
            tests[(data, 'default')] = False
            
        # test with no-chunk
    
        subprocess.run(['../mpfree_sequential', '--no-chunk', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'no-chunk')] = True
        else:
            tests[(data, 'no-chunk')] = False
            
        # test with bit_tree_pivot_column
    
        subprocess.run(['../mpfree_sequential', '--bit_tree_pivot_column', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'bit_tree_pivot_column')] = True
        else:
            tests[(data, 'bit_tree_pivot_column')] = False
            
        # test with clearing
        
        precomputed_output_file = 'min_' + data + '_clearing.firep'
        subprocess.run(['../mpfree_sequential', '--clearing', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'clearing')] = True
        else:
            tests[(data, 'clearing')] = False
            
        if all([tests[key] for key in tests.keys() if key[0] == data]):
            print('... all passed.')
            
    if all(list(tests.values())):
        print('mpfree_sequential passed all tests.')
    else:
        for key in tests.keys():
            if tests[key] == False:
                print('mpfree_sequential output on ' + key[0] + ' with option ' + key[1] + \
                      ' does not match precomputed output')
                
else:
    
    print('Could not find mpfree_sequential')
            
# testing mpfree
            
if os.path.isfile('../mpfree'):
    
    print('Testing mpfree')
    
    tests = {}
    
    for data in test_data:
        
        print('Testing ' + data)
        
        input_file = '../sample_files/' + data + '.firep'
        output_file = 'min_' + data + '_test.firep'
        
        # test with default options
        
        precomputed_output_file = 'min_' + data + '.firep'
        subprocess.run(['../mpfree', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'default')] = True
        else:
            tests[(data, 'default')] = False
            
        # test with no-chunk
    
        subprocess.run(['../mpfree', '--no-chunk', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'no-chunk')] = True
        else:
            tests[(data, 'no-chunk')] = False
            
        # test with bit_tree_pivot_column
    
        subprocess.run(['../mpfree', '--bit_tree_pivot_column', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'bit_tree_pivot_column')] = True
        else:
            tests[(data, 'bit_tree_pivot_column')] = False
            
        # test with clearing
        
        precomputed_output_file = 'min_' + data + '_clearing.firep'
        subprocess.run(['../mpfree', '--clearing', input_file, output_file])
        
        if open(precomputed_output_file,'r').read() == open(output_file,'r').read():
            tests[(data, 'clearing')] = True
        else:
            tests[(data, 'clearing')] = False
            
        if all([tests[key] for key in tests.keys() if key[0] == data]):
            print('... all passed.')
            
    if all(list(tests.values())):
        print('mpfree passed all tests.')
    else:
        for key in tests.keys():
            if tests[key] == False:
                print('mpfree output on ' + key[0] + ' with option ' + key[1] + \
                      ' does not match precomputed output')
                
else:
    
    print('Could not find mpfree')