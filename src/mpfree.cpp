/* Copyright 2020 TU Graz
   Author: Michael Kerber
   
   This file is part of mpfree
   
   mpfree is free software: you can redistribute it and/or modify
   it under the terms of the GNU Lesser General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.
   
   mpfree is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU Lesser General Public License for more details.
   
   You should have received a copy of the GNU Lesser General Public License
   along with mpfree.  If not, see <https://www.gnu.org/licenses/>.*/

// The reading from input uses strtok_r as a thread-save
// version of strtok. If this function is not enabled,
// that part of code is not parallelized, which results
// in a slight performance penalty
//
// Disabled, because it causes problems on server... 
#ifndef STRINGTOK_R_AVAILABLE
#define STRINGTOK_R_AVAILABLE 0
#endif


#include <iostream>
#include <fstream>

#ifndef MPFREE_TIMERS
#define MPFREE_TIMERS 0
#endif

#if MPFREE_TIMERS
#include <mpfree/boost_timers.h>
#endif


#include <sys/time.h>
#include <sys/resource.h>

long mem_info() {
  struct rusage usage;
  getrusage(RUSAGE_SELF,&usage);
  return usage.ru_maxrss;
}



#include <mpfree/mpfree.h>
#include <mpp_utils/Graded_matrix.h>

void print_help_message(char* arg0) {

  std::cout << "Usage: " << arg0 << " [OPTIONS] <INPUT_FILE> <OUTPUT_FILE>\n\n";

  std::cout << "Computes the minimial presentation of a sequence of free persistence modules, either in Rivet firep format or in scc2020 format. The output is a sequence of free persistence modules of length one. The output file can be omitted, in which case no output is created\n\n";

  std::cout << "Options:\n";
  std::cout << "-c                      - checks whether the input is in the right format and exits\n";
  std::cout << "--resolution            - computes the full free resolution. In this case, the output is always in scc2020 format\n";
  std::cout << "--rivet-format          - yields the output as a firep file as in the RIVET library (by default, it is a scc2020 file)\n";
  std::cout << "--no-chunk              - disables chunk-preprocessing (by default, it is enabled)\n";
  std::cout << "--clearing              - enables clearing optimization (by default, it is disabled)\n";
  std::cout << "--bit_tree_pivot_column - uses the column type bit_tree_pivot_column from phat. The default is vector_vector from phat\n";
  std::cout << "--dim=p                 - Computes the minimal presentation for the p-th, (p+1)-st, and (p+2)-nd module (starting the count with 1). Only useful with scc2020 input format (by default, p=1)\n";
  std::cout << "-v                      - prints status messages during the execution\n";
  std::cout << "-h --help               - prints this message\n\n";
}
    
    



int main(int argc, char** argv) {

  // parse command line

  bool only_check=false;

  bool use_chunk=true;
  bool use_clearing=false;
  bool input_file=false;
  bool output_file=false;
  bool verbose_output=false;
  bool output_in_scc_format=true;

  bool use_vector_vector=true;
  bool use_bit_tree_pivot_column=false;

  bool resolution=false;

  bool print_help=false;

  std::string infile;
  std::string outfile="";

  int dim=1;

  for(int i=1;i<argc;i++) {
    
    std::string arg(argv[i]);
    
    if(arg=="-c") {
      only_check=true;
    } else if(arg=="--no-chunk") {
      use_chunk=false;
    } else if(arg=="--clearing") {
      use_clearing=true; 
    } else if(arg=="-v") {
      verbose_output=true;
    } else if(arg=="-h" || arg=="--help") {
      print_help=true;
    } else if(arg=="--rivet-format") {
      output_in_scc_format=false;
    } else if(arg=="--bit_tree_pivot_column") {
      use_vector_vector=false;
      use_bit_tree_pivot_column=true;
    } else if(arg=="--resolution") {
      resolution=true;
    } else if(arg.rfind("--dim=",0)==0) {
      dim=atoi(arg.substr(6).c_str());
      std::cout << "Dimension set to " << dim << std::endl;
    } else {
      if(arg[0]=='-') {
	std::cout << "Unrecognized option: " << arg << std::endl;
	std::exit(1);
      }
      if(! input_file) {
	infile=arg;
	input_file=true;
      } else if(! output_file) {
	outfile=arg;
	output_file=true;
      } else {
	std::cerr << "Ignoring argument " << arg << std::endl;
      }
    }
  }

  if(print_help) {
    print_help_message(argv[0]);
    std::exit(0);
  }

  if(! input_file) {
    std::cerr << "Error: No input file given\n\n";
    print_help_message(argv[0]);
    std::exit(1);
  }

  { 
    // Check whether input file exists
    if(FILE *file = fopen(infile.c_str(),"r")) {
      fclose(file);
    } else {
      std::cerr << "Input file " << infile << " does not exist, exiting" << std::endl;
      std::exit(1);
    }
  }

  if(verbose_output) {
    mpfree::verbose=true;
  }
  
  if(only_check) {
    typedef mpp_utils::Graded_matrix<phat::vector_vector> GrMat;

    std::vector<GrMat> matrices;
  
    std::ifstream ifstr(infile);
    scc::Scc<> parser(ifstr);
    
    mpp_utils::create_graded_matrices_from_scc2020(parser,1,2,matrices);

    GrMat& GM1=matrices[0];
    GrMat& GM2=matrices[1];

    check_grade_sanity(GM1);
    check_boundaries(GM1,"first");
    check_boundaries(GM2,"second");
    std::cerr << "Input is valid, exiting..." << std::endl;
    std::exit(0);
  }

#if MPFREE_TIMERS
  mpfree::initialize_timers();
  mpfree::overall_timer.start();
#endif


  if(use_vector_vector) {
    typedef mpp_utils::Graded_matrix<phat::vector_vector> Graded_matrix;
    if(resolution) {
      mpfree::compute_free_resolution<Graded_matrix>(infile,outfile,dim,use_chunk,use_clearing);
    } else {
      mpfree::compute_minimal_presentation<Graded_matrix>(infile,outfile,output_in_scc_format,dim,use_chunk,use_clearing);
    }
  } else if(use_bit_tree_pivot_column) {
    typedef mpp_utils::Graded_matrix<phat::bit_tree_pivot_column> Graded_matrix;
    if(resolution) {
      mpfree::compute_free_resolution<Graded_matrix>(infile,outfile,dim,use_chunk,use_clearing);
    } else {
      mpfree::compute_minimal_presentation<Graded_matrix>(infile,outfile,output_in_scc_format,dim,use_chunk,use_clearing);
    }
  } else {
    std::cerr << "No column type is specified" << std::endl;
    std::exit(1);
  }

#if MPFREE_TIMERS
  if(mpfree::verbose) mpfree::print_timers(double(mpfree::overall_timer.elapsed().wall),true,resolution);
#endif
  

}
