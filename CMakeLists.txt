project( Code_ )

cmake_minimum_required(VERSION 3.9)

set(CMAKE_CXX_STANDARD 14)

if (NOT CMAKE_VERBOSE_MAKEFILE)
   set(CMAKE_VERBOSE_MAKEFILE ON)
endif()

if (NOT CMAKE_BUILD_TYPE)
    set(CMAKE_BUILD_TYPE "Release")
endif()

if (NOT PHAT_INCLUDE_DIR)
    set(PHAT_INCLUDE_DIR "phat_mod/include")
endif()

if (NOT MPP_UTILS_INCLUDE_DIR)
    set(MPP_UTILS_INCLUDE_DIR "mpp_utils_mod/include")
endif()

if (NOT SCC_INCLUDE_DIR)
    set(SCC_INCLUDE_DIR "scc_mod/include")
endif()


include_directories(${PHAT_INCLUDE_DIR})
include_directories(${MPP_UTILS_INCLUDE_DIR})
include_directories(${SCC_INCLUDE_DIR})
include_directories(include)

FIND_PACKAGE(OpenMP)

find_package(Boost OPTIONAL_COMPONENTS timer)

if(Boost_TIMER_FOUND)
    add_compile_options(-DMPFREE_TIMERS=1)
    link_libraries(Boost::timer)
endif()

#Note: Instead of using "${OpenMP_CXX_FLAGS}", it is recommend to use
#"OpenMP::OpenMP_CXX" as target, but it does not work on my local machine

if(OpenMP_CXX_FOUND)
    add_executable(mpfree "src/mpfree.cpp")
    target_compile_options(mpfree PUBLIC ${OpenMP_CXX_FLAGS})
    target_link_libraries(mpfree ${OpenMP_CXX_FLAGS})
endif()

add_executable(mpfree_sequential "src/mpfree.cpp")
